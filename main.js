"use strict";
 
const Discord = require(`discord.js`);
const client = new Discord.Client();
const db = require(`./db.js`);
const pool = db.getPool();
 
client.on(`ready`, () => {
  console.log(`Logged in as ${client.user.tag}!`);
});
 
client.on(`message`, msg => {
  if (msg.author.bot) {
    return;
  }
  if (msg.content.startsWith(`.`)) {
    const segments = msg.content.toLowerCase().split(` `);
    const commandName = segments[0].replace(`.`, ``);
    const args = segments.slice(1);
    const string = msg.content.split(` `).slice(1).join(` `);
    if (commandName === `gracz`) {
      if (args.length > 0) {
        pool.query(`SELECT * FROM players WHERE playerName = ?`, [string], (err, results) => {
          if (err) {
            msg.reply(`DataBase error`); 
            console.error(err);
            return;
          }
          if (results.length < 1) {
            msg.reply('nie znaleziono gracza w bazie danych');
            return;
          }
          const player = new Discord.MessageEmbed().setTitle(`Statystyki gracza ${results[0].playerName}`).setColor(`#FF0000`).setFooter(`AriaBot by assasyn`).setDescription(`Ranking: 100\nŚmierci: 0\nZabójstwa: ${results[0].deaths}\nAsysty: ${results[0].assists}\nCoinsy: ${results[0].coins}`);
          msg.channel.send(player);
        }); 
      } else {
        msg.reply(`Podaj nick gracza!`);
      }
    }
    if (commandName === `serwer` || commandName === `server` || commandName === `info` || commandName === `informacje`) {
      const serverinformation = new Discord.MessageEmbed().setTitle(`Informacje o AriaNetwork:`).setColor(`#FF0000`).setFooter(`AriaBot by assasyn`).setDescription(`AriaNetwork to zrzeszenie serwerow AriaPVP.net oraz MinecraftPE.pl.\nWlascicielem zarowno jednego jak i drugiego serwera jest assasyn.\n Oba serwery są na wersje 1.14.60 Minecraft Bedrock/Pocket.\nGłównym developerem jest Fenek912 oraz Liamdj23.\nProjekt AriaNetwork jest tworzony od kilku miesięcy i dokładamy wszelkich\nstarań aby był on dopracowany i innowacyjny.`);
      msg.channel.send(serverinformation);
    }
    if (commandName === `help` || commandName === `?` || commandName === `pomoc`) {
      const help = new Discord.MessageEmbed().setTitle(`test:`).setColor(`#FF0000`).setFooter(`AriaBot by assasyn`).setDescription(`testtest`);
      msg.channel.send(help);
    }
    if (commandName === `test` || commandName === `test1` || commandName === `test2`) {
      const test = new Discord.MessageEmbed().setTitle(`Lista komend bota:`).setColor(`#FF0000`).setFooter(`AriaBot by assasyn`).setThumbnail('https://cdn.discordapp.com/attachments/528678706756190244/708815045898928308/90637915_2559471390932794_4833033896830435328_n.png').setDescription(`.gracz <nick> - statystyki gracza z serwera\n.serwer - informacje o serwerch`);
      msg.channel.send(test);
    }
    if (commandName === `faq`) {
      const test = new Discord.MessageEmbed()
      .setTitle(`FAQ:`)
      .setColor(`#FF0000`)
      .setFooter(`AriaBot by assasyn`)
      .setThumbnail('https://images-ext-1.discordapp.net/external/jw29bU6H3FC537jgX42lGIvLQdg4mxTIQ6rsZVtSNvI/%3Fsize%3D1024/https/cdn.discordapp.com/avatars/301024178054627329/46dc988bb2cabfe9895185007c325b56.webp?width=560&height=560')
      .setDescription(`*Kto jest autorem bota?*\n- Autorem bota jest 17-mnasto letni, początkujący programista z pomocą użytkownika Fenek912#5713\n*Ile powstawał bot?*\n- Bot powstawał 2dni.`);
      msg.channel.send(test);
    }
  }
});
 
client.login(`NzA4MzA5NzE0Njc2NDgyMTM4.XrXH0Q.aPd-IHOKtB_t8_EyWdGrppfuH6U`);
