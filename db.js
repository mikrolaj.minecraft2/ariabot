"use strict";

const mysql = require('mysql');
const settings = require('./settings.js');

let pool;

module.exports.getPool = () => {
    if (pool) return pool;

    pool = mysql.createPool({
        host: settings.get.databaseHost,
        user: settings.get.databaseUser,
        password: settings.get.databasePass,
        database: settings.get.databaseName
    });

    return pool;
};
